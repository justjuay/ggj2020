﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JTK.Utitlities
{
    public static class UIDCreater
    {
        public static string CreateUID ()
        {
            string UID = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
            return UID.Replace("==", string.Empty);
            //UID = Guid.NewGuid().ToString("N");
        }

    }
}

