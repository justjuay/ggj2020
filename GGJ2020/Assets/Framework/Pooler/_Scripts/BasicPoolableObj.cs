﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace JTK
{
    public class BasicPoolableObj : BasePoolableObj
    {
        

        [SerializeField] UnityEvent OnEnterPoolEvent;
        [SerializeField] UnityEvent OnExitPoolEvent;
        [SerializeField] UnityEvent OnDestroyEvent;

        public override void OnEnterPool ()
        {
            OnEnterPoolEvent.Invoke();
        }

        public override void OnExitPool ()
        {
            OnExitPoolEvent.Invoke();
        }

        private void OnDestroy ()
        {
            OnDestroyEvent.Invoke();
        }

        // Start is called before the first frame update
        void Start ()
        {

        }

        // Update is called once per frame
        void Update ()
        {

        }
    }
}

