﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JTK
{
    public abstract class BasePoolableObj : MonoBehaviour, IPoolable
    {
        [UID] [SerializeField] public string UID = JTK.Utitlities.UIDCreater.CreateUID();
        
        public string GetUID() { return UID; }
        public GameObject GetGameObject() { return this.gameObject; }

        public abstract void OnEnterPool ();
        public abstract void OnExitPool ();
        

        public void OnDestroy ()
        {
            
        }

    }
}

