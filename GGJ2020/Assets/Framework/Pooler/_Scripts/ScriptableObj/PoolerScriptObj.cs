﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JTK
{
    [CreateAssetMenu(fileName = "PoolerData", menuName = "JTK/PoolerData")]
    public class PoolerScriptObj : ScriptableObject
    {
        public List<SinglePoolData> data;
    }

    [System.Serializable]
    public class SinglePoolData
    {
        public int initialSpawn;
        public GameObject prefab;
    }
}
