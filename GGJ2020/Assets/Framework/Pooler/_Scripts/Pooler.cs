﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JTK
{
    public interface IPoolable
    {
        string GetUID ();
        void OnEnterPool ();
        void OnExitPool ();
        void OnDestroy ();
        GameObject GetGameObject ();
    }

    public class Pooler : MonoBehaviour
    {

        [SerializeField] PoolerScriptObj initialPoolObj;
        [ReadOnly] [SerializeField] List<string> IDList = new List<string>();

        [Header("Debug")]
        [SerializeField] bool DebugOn = false;

        static Pooler _instance;
        public static Pooler Instance
        {
            get
            {
                if (_instance == null) Debug.LogError("[Pooler] No Instance!");
                return _instance;
            }
        }

        private Dictionary<string, Queue<IPoolable>> poolDictionary = new Dictionary<string, Queue<IPoolable>>();


        private void Awake ()
        {
            if (_instance == null)
                _instance = this;
            else
                Debug.LogError("[Pooler] Another Instance of Pooler exists");
        }
        // Start is called before the first frame update
        void Start ()
        {
            if (initialPoolObj == null) return;

            foreach (SinglePoolData poolData in initialPoolObj.data)
            {
                if (poolData.prefab != null && poolData.initialSpawn > 0)
                {
                    IPoolable poolable = poolData.prefab.GetComponent<IPoolable>();
                    if (poolable != null)
                    {
                        for (int i = 0; i < poolData.initialSpawn; i++)
                        {
                            GameObject obj = GameObject.Instantiate(poolData.prefab);
                            ReturnToPool(obj.GetComponent<IPoolable>());
                        }
                        if (DebugOn)
                            Debug.LogFormat("[Pooler] {0} cloned {1} instances", poolData.prefab.name, poolData.initialSpawn);
                    }
                    else
                    {
                        Debug.LogWarningFormat("[Pooler] {0} does not implement IPoolable, no dictinary entry created", poolData.prefab.name);
                    }
                }
            }
        }

        public static void ReturnToPool (IPoolable _object)
        {
            if (!Instance.poolDictionary.ContainsKey(_object.GetUID()))
            {
                CreateDictionaryEntry(_object);
            }

            Queue<IPoolable> queue;
            Instance.poolDictionary.TryGetValue(_object.GetUID(), out queue);
            
            _object.OnEnterPool();
            queue.Enqueue(_object);

            if (Instance.DebugOn) Debug.LogFormat("[Pooler] {0} has been added to pool, {1} in pool", _object.GetUID(), queue.Count);
        }

        public static void GetFromPool (string _UID, out IPoolable _object)
        {
            Queue<IPoolable> queue;
            if (Instance.poolDictionary.TryGetValue(_UID, out queue))
            {
                if (queue.Count > 0)
                {
                    _object = queue.Dequeue();
                    _object.OnExitPool();
                    if (Instance.DebugOn)
                        Debug.LogFormat("[Pooler] {0} has been remove from pool, {1} remaining in pool", _UID, queue.Count);

                    return;
                }
            }

            if (Instance.DebugOn)
                Debug.LogFormat("[Pooler] {0} has no instance in pool, return null", _UID);
            _object = null;
        }

        public static bool ClearPool (string _UID)
        {
            Queue<IPoolable> queue;
            if (Instance.poolDictionary.TryGetValue(_UID, out queue))
            {
                IPoolable obj;
                for (int i = 0; i < queue.Count; i++)
                {
                    obj = queue.Dequeue();
                    obj.OnDestroy();
                }
                if (Instance.DebugOn)
                    Debug.LogFormat("[Pooler] {0} has been destroyed from pool, {1} remaining in pool", _UID, queue.Count);
                return true;
            }
            else
                return false;
        }

        public static void CreateDictionaryEntry (IPoolable _object)
        {
            if (!Instance.poolDictionary.ContainsKey(_object.GetUID()))
            {
                Instance.poolDictionary.Add(_object.GetUID(), new Queue<IPoolable>());
                Instance.IDList.Add(_object.GetUID());

                if (Instance.DebugOn)
                    Debug.LogFormat("[Pooler] Create dictionary entry for {0}", _object.GetUID());
            }
            else
                Debug.LogErrorFormat("[Pooler] Dictionary already contain Key: {0}", _object.GetUID());
        }
    }

}

