﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JTK;

public class SpawnerController : MonoBehaviour
{
    [SerializeField] BasePoolableObj objToSpawn;
    [SerializeField] bool isActive = true;

    [ReadOnly] [SerializeField] float timer = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isActive)
        {

            if (timer <= 0)
            {
                IPoolable spawnObj = null;
                GameObject obj = null;

                Pooler.GetFromPool(objToSpawn.GetUID(), out spawnObj);
                if (spawnObj == null)
                {
                    obj = Instantiate(objToSpawn.gameObject);
                }
                else
                {
                    obj = spawnObj.GetGameObject();
                }

                obj.transform.position = new Vector3(Random.Range(0, 100), Random.Range(0, 100), 0);
                timer = Random.Range(0.5f, 2f);
            }
            timer -= Time.deltaTime;
        }

    }
}
