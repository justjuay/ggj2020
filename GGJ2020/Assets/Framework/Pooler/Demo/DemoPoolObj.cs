﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using JTK;

public class DemoPoolObj : BasePoolableObj
{
    bool isActive = true;
    float timer = 0;
    const float maxTime = 10;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isActive)
        {
            timer += Time.deltaTime;
            this.transform.Rotate(new Vector3(0, 0, 360 * Time.deltaTime));
            if (timer >= maxTime)
                Pooler.ReturnToPool(this);
        }
    }

    public override void OnEnterPool ()
    {
        this.gameObject.SetActive(false);
        isActive = false;
    }

    public override void OnExitPool ()
    {
        this.gameObject.SetActive(true);
        isActive = true;
        timer = 0;
    }
}
