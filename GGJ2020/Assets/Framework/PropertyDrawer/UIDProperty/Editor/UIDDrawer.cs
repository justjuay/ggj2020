﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomPropertyDrawer(typeof(UIDAttribute))]

public class UIDDrawer : PropertyDrawer
{
    const int btnWidth = 40;
    const int IDWidth = 500;

    public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
    {
        return EditorGUI.GetPropertyHeight(property, label, true);
    }

    public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
    {
        label = EditorGUI.BeginProperty(position, label, property);
        position = EditorGUI.PrefixLabel(position, label);

        EditorGUI.BeginChangeCheck();

        Rect btnPos = new Rect(position.position, new Vector2(btnWidth, position.height));
        if (GUI.Button(btnPos, "New"))
        {
            property.stringValue = JTK.Utitlities.UIDCreater.CreateUID();
            //property.serializedObject.ApplyModifiedProperties();
        }

        Rect IDPos = new Rect(position.position + new Vector2(btnWidth, 0), new Vector2(IDWidth, position.height));
        EditorGUI.SelectableLabel(IDPos, property.stringValue);

        if (EditorGUI.EndChangeCheck())
            property.serializedObject.ApplyModifiedProperties();

        EditorGUI.EndProperty();
    }
}
