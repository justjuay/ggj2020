﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName="AudioManager", menuName="JTK/Audio/AudioManager", order = 0)]
public class AudioManagerScriptObj : ScriptableObject
{
    static AudioManagerScriptObj _instance;
    public static AudioManagerScriptObj Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("[AUDIO MANAGER] No Instance. Please AudioManagerScriptObj.Initialize()");
            }
            return _instance;
        }
        private set { }
    }

    List<AudioSource> audioSourcePool;
    public GameObject poolObj { get; private set; }

    public void Initialize (GameObject _audioPoolingObj)
    {
        if (poolObj == null) poolObj = _audioPoolingObj;
        if (_instance == null) _instance = this;

        audioSourcePool = new List<AudioSource>();

        DontDestroyOnLoad(poolObj);
    }

    public AudioSource GetAudioSource ()
    {
        AudioSource src = audioSourcePool.Find((AudioSource obj) => !obj.isPlaying);
        if (src == null)
        {
            src = CreateNewAudioSource();
            audioSourcePool.Add(src);
        }
        return src;
    }

    public bool RemoveSourceFromPool (AudioSource _src)
    {
        return audioSourcePool.Remove(_src);
    }

    AudioSource CreateNewAudioSource ()
    {
        AudioSource src = null;
        if (poolObj != null)
        {
            GameObject GO = new GameObject("AudioSource");
            GO.transform.parent = poolObj.transform;
            src = GO.AddComponent<AudioSource>();
            src.playOnAwake = false;
        }
        return src;
    }


}
