﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

namespace JTK.Audio
{
    public abstract class AbstractClipScriptObj : ScriptableObject
    {
        [Header("Channel")]
        public AbstractChannelScriptObj channel;

        public abstract AudioClip GetClip ();

        public virtual void Play2D ()
        {
            channel.Play2D(GetClip(), null);
        }

        public virtual void Play2D (ref AudioSource _src)
        {
            _src = channel.Play2D(GetClip(), _src);
        }
    }

}
