﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JTK.Audio
{
    [CreateAssetMenu(fileName = "SingleClip", menuName = "JTK/Audio/Clips/SingleClip", order = 0)]
    public class SingleClipScriptObj : AbstractClipScriptObj
    {
        [Header("Clip")]
        [SerializeField] protected AudioClip clip;

        public override AudioClip GetClip ()
        {
            return clip;
        }

    }
}



