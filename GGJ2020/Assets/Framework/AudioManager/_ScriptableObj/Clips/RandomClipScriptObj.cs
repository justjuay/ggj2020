﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JTK.Audio
{
    [CreateAssetMenu(fileName = "RandomClip", menuName = "JTK/Audio/Clips/RandomClip", order = 1)]
    public class RandomClipScriptObj : AbstractClipScriptObj
    {
        [System.Serializable] public class DataSet
        {
            [Range(0, 1)]
            public float weight;
            public AudioClip clip;
        }

        [Header("Clips")]
        [SerializeField] protected List<DataSet> randomSet;

        float totalWeight = -1;

        public override AudioClip GetClip ()
        {
            if (totalWeight <= 0)
            {
                totalWeight = 0;
                foreach (DataSet set in randomSet)
                    totalWeight += set.weight;
            }

            float rand = Random.Range(0, totalWeight);

            for (int i = 0; i < randomSet.Count; i++)
            {
                if (rand <= randomSet[i].weight)
                    return randomSet[i].clip;
            }
            return randomSet[randomSet.Count - 1].clip;
        }

    }
}
