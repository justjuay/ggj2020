﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace JTK.Audio
{
    public abstract class AbstractChannelScriptObj : ScriptableObject
    {
        [Header("Mixer")]
        public AudioMixer mixer;
        public AudioMixerGroup mixerGroup;

        [Header("Volume")]
        protected string volumeParameter;
        public float volume
        {
            get
            {
                float dBvol = 0f;
                mixer.GetFloat(volumeParameter, out dBvol);
                float linearVol = Mathf.Clamp01(Mathf.Pow(10.0f, dBvol / 20.0f));
                if (linearVol <= 0.0001f) return 0;
                else return linearVol;
            }
            set
            {
                // 0.0001f linear = -80dB
                float dbVol = 20.0f * Mathf.Log10(Mathf.Clamp(value, 0.0001f, 1.0f));
                mixer.SetFloat(volumeParameter, dbVol);
            }
        }

        
        public abstract AudioSource Play2D (AudioClip _clip, AudioSource _source);

        //public abstract bool Play3D (string _UID, Vector3 _location, AudioSource _source);

    }
}

