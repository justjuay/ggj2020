﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JTK.Audio
{
    [CreateAssetMenu(fileName = "MultiSourceChannel", menuName = "JTK/Audio/Channels/MultiSource", order = 11)]
    public class MultiSourceChannelScriptObj : AbstractChannelScriptObj
    {
        public override AudioSource Play2D (AudioClip _clip, AudioSource _source = null)
        {
            AudioSource source = _source == null ? AudioManagerScriptObj.Instance.GetAudioSource() : _source;

            source.clip = _clip;
            source.outputAudioMixerGroup = mixerGroup;
            source.loop = false;
            source.Play();

            return source;
        }

    }

}
