﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JTK.Audio
{
    [CreateAssetMenu(fileName = "SingleSourceChannel", menuName = "JTK/Audio/Channels/SingleSource", order = 0)]
    public class SingleSourceChannelScriptObj : AbstractChannelScriptObj
    {
        [Header("Single Source Settings")]
        public bool isLooping;

        AudioSource singleSource;

        public override AudioSource Play2D (AudioClip _clip, AudioSource _source = null)
        {
            if (singleSource == null)
            {
                singleSource = _source == null ? AudioManagerScriptObj.Instance.GetAudioSource() : _source;
                AudioManagerScriptObj.Instance.RemoveSourceFromPool(singleSource);
            }

            if (singleSource.isPlaying)
                singleSource.Stop();

            singleSource.clip = _clip;
            singleSource.outputAudioMixerGroup = mixerGroup;
            singleSource.loop = isLooping;
            singleSource.Play();
            
            return singleSource;
        }
        
    }
}

