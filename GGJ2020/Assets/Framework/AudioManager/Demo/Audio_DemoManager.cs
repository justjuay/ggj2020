﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using JTK.Audio;

namespace JTK.Audio.Demo
{
    public class Audio_DemoManager : MonoBehaviour
    {
        [SerializeField] AudioManagerScriptObj audioManager;
        // Start is called before the first frame update
        void Start ()
        {
            audioManager.Initialize(this.gameObject);
        }
        
    }

}

