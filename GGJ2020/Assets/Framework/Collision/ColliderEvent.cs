﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable] public class UnityEvent_Collision : UnityEvent<Collision> { }
[System.Serializable] public class UnityEvent_Collider : UnityEvent<Collider> { }

public class ColliderEvent : MonoBehaviour {
    [Header("SETTINGS")]
    [SerializeField] LayerMask collisionMask;
    [SerializeField] bool isDebugOn = false;

    [Header("ON TRIGGER EVENTS")]
    public UnityEvent_Collider TriggerEnterResponse;
    public UnityEvent_Collider TriggerExitResponse;
    public event System.Action<Collider> TriggerEnterEvent = delegate { };
    public event System.Action<Collider> TriggerExitEvent = delegate { };

    [Header("ON COLLISION EVENTS")]
    public UnityEvent_Collision CollisionEnterResponse;
    public UnityEvent_Collision CollisionExitResponse;
    public event System.Action<Collision> CollisionEnterEvent = delegate { };
    public event System.Action<Collision> CollisionExitEvent = delegate { };

    public void OnTriggerEnter (Collider _other)
    {
        if (collisionMask == (collisionMask | 1 << _other.gameObject.layer))
        {
            if (isDebugOn && (TriggerEnterEvent != null || TriggerEnterResponse.GetPersistentEventCount() > 0))
                Debug.Log("[" + this.name +  "] TRIGGER ENTER EVENT ON [" + _other.name + "]");

            if (TriggerEnterEvent != null)
                TriggerEnterEvent.Invoke(_other);

            TriggerEnterResponse.Invoke(_other);
        }
    }

    public void OnTriggerExit (Collider _other)
    {
        if (collisionMask == (collisionMask | 1 << _other.gameObject.layer))
        {
            if (isDebugOn && (TriggerExitEvent != null || TriggerExitResponse.GetPersistentEventCount() > 0))
                Debug.Log("[" + this.name + "] TRIGGER EXIT EVENT ON [" + _other.name + "]");

            if (TriggerExitEvent != null)
                TriggerExitEvent.Invoke(_other);
            
            TriggerExitResponse.Invoke(_other);
        }
    }

    public void OnCollisionEnter (Collision _collision)
    {
        if (collisionMask == (collisionMask | 1 << _collision.gameObject.layer))
        {
            if (isDebugOn && (CollisionEnterEvent != null || CollisionEnterResponse.GetPersistentEventCount() > 0))
                Debug.Log("[" + this.name + "] COLLISION ENTER EVENT ON [" + _collision.rigidbody.name + "]");

            if (CollisionEnterEvent != null)
                CollisionEnterEvent.Invoke(_collision);

            CollisionEnterResponse.Invoke(_collision);
        }
    }

    public void OnCollisionExit (Collision _collision)
    {
        if (collisionMask == (collisionMask | 1 << _collision.gameObject.layer))
        {
            if (isDebugOn && (CollisionExitEvent != null || CollisionEnterResponse.GetPersistentEventCount() > 0))
                Debug.Log("[" + this.name + "] COLLISION EXIT EVENT ON [" + _collision.rigidbody.name + "]"); 

            if (CollisionExitEvent != null)
                CollisionExitEvent.Invoke(_collision);

            CollisionExitResponse.Invoke(_collision);
        }
    }

}
