﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JTK
{
    [System.Serializable]
    public class IntReference
    {
        public bool UseConstant = true;
        public int ConstantValue = 0;
        public IntVariable Variable = null;

        public int Value
        {
            get { return UseConstant ? ConstantValue : Variable.Value; }
            private set { }
        }
    }
}