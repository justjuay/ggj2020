﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace JTK
{
    [CustomPropertyDrawer(typeof(IntReference))]
    public class IntReferenceEditorScript : PropertyDrawer
    {
        // Options to display in the popup selection
        protected readonly string[] popupOptions = { "Use Constant", "Use Variable" };
        // Cached style to use to draw the popup button
        protected GUIStyle popupStyle;

        public override void OnGUI (Rect _position, SerializedProperty _property, GUIContent _label)
        {
            if (popupStyle == null)
            {
                popupStyle = new GUIStyle(GUI.skin.GetStyle("PaneOptions"));
                popupStyle.imagePosition = ImagePosition.ImageOnly;
            }

            _label = EditorGUI.BeginProperty(_position, _label, _property);
            _position = EditorGUI.PrefixLabel(_position, _label);

            EditorGUI.BeginChangeCheck();

            // Get properties
            SerializedProperty useConstant = _property.FindPropertyRelative("UseConstant");
            SerializedProperty constantValue = _property.FindPropertyRelative("ConstantValue");
            SerializedProperty variable = _property.FindPropertyRelative("Variable");

            // Calculate rect for configuration button
            Rect buttonRect = new Rect(_position);
            buttonRect.yMin += popupStyle.margin.top;
            buttonRect.width = popupStyle.fixedWidth + popupStyle.margin.right;
            _position.xMin = buttonRect.xMax;

            // Store old indent level and set it to 0, the PrefixLabel takes care of it
            int indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            int result = EditorGUI.Popup(buttonRect, useConstant.boolValue ? 0 : 1, popupOptions, popupStyle);

            useConstant.boolValue = result == 0;

            EditorGUI.PropertyField(_position, useConstant.boolValue ? constantValue : variable, GUIContent.none);

            if (EditorGUI.EndChangeCheck())
                _property.serializedObject.ApplyModifiedProperties();

            EditorGUI.indentLevel = indent;
            EditorGUI.EndProperty();
        }
    }
}

