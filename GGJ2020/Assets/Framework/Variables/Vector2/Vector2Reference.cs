﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JTK
{
    [System.Serializable]
    public class Vector2Reference
    {
        public bool UseConstant = true;
        public Vector2 ConstantValue = Vector2.zero;
        public Vector2Variable Variable = null;

        public Vector2 Value
        {
            get { return UseConstant ? ConstantValue : Variable.Value; }
            private set { }
        }
    }
}


