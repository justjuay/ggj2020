﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEditor;
using UnityEditorInternal;

namespace JTK
{
    [CustomEditor(typeof(Vector2Variable))]
    public class Vector2VariableEditorScript : Editor
    {
        Vector2Variable thisTarget;
        ReorderableList eventHandlerList;

        public void OnEnable ()
        {
            thisTarget = (Vector2Variable)target;

            eventHandlerList = new ReorderableList(serializedObject, serializedObject.FindProperty("OnChangeEvents"), true, true, true, true);
            eventHandlerList.drawHeaderCallback = rect =>
            {
                EditorGUI.LabelField(rect, "OnValueChange Event List", EditorStyles.boldLabel);
            };

            eventHandlerList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
            {
                var element = eventHandlerList.serializedProperty.GetArrayElementAtIndex(index);
                EditorGUI.ObjectField(new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight), element, GUIContent.none);
            };
        }

        public override void OnInspectorGUI ()
        {
            if (thisTarget == null)
                thisTarget = (Vector2Variable)target;

            base.OnInspectorGUI();


            if (thisTarget.ChangeValueAlert)
            {
                serializedObject.Update();
                eventHandlerList.DoLayoutList();
                serializedObject.ApplyModifiedProperties();
            }
        }

    }
}

