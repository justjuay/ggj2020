﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace JTK
{
    [CreateAssetMenu(menuName = "JTK/Framework/Variables/Float")]
    public class FloatVariable : ScriptableObject
    {
        [Header("VALUE")]
        [SerializeField] private float _Value;
        public float Value
        {
            get { return _Value; }
            set
            {
                _Value = value;
                if (ChangeValueAlert)
                {
                    for (int i = 0; i < OnChangeBasicEvents.Count; i++)
                        OnChangeBasicEvents[i].Raise();
                }
            }
        }

        #if UNITY_EDITOR
        [Multiline] public string Description = "";
        #endif

        [Header("Value Change Event")]
        public bool ChangeValueAlert = false;
        public List<BasicEventHandler> OnChangeBasicEvents;

        public FloatVariable (float _value)
        {
            OnChangeBasicEvents = new List<BasicEventHandler>();
            Value = _value;
        }

        public void RegisterEventHandler (BasicEventHandler _handler)
        {
            OnChangeBasicEvents.Add(_handler);
        }

        public bool UnregisterEventHandler (BasicEventHandler _handler)
        {
            return OnChangeBasicEvents.Remove(_handler);
        }
        
    }
    
}

