﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JTK
{
    [System.Serializable]
    public class FloatReference
    {
        public bool UseConstant = true;
        public float ConstantValue = 0f;
        public FloatVariable Variable = null;

        public float Value
        {
            get { return UseConstant ? ConstantValue : Variable.Value; }
            private set { }
        }
    }
}
