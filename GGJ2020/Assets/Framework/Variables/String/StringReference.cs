﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JTK
{
    [System.Serializable]
    public class StringReference
    {
        public bool UseConstant = true;
        public string ConstantValue = "";
        public StringVariable Variable = null;

        public string Value
        {
            get { return UseConstant ? ConstantValue : Variable.Value; }
            private set { }
        }
    }
}