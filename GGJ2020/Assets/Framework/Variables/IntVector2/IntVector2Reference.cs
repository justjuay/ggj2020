﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JTK
{
    [System.Serializable]
    public class IntVector2Reference
    {
        public bool UseConstant = true;
        public Vector2Int ConstantValue = Vector2Int.zero;
        public IntVector2Variable Variable = null;

        public Vector2Int Value
        {
            get { return UseConstant ? ConstantValue : Variable.Value; }
            private set { }
        }
    }
}

