﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JTK
{
    [CreateAssetMenu(menuName = "JTK/Framework/Variables/IntVector2")]
    public class IntVector2Variable : ScriptableObject
    {
        [Header("VALUE")]
        [SerializeField] private Vector2Int _Value = Vector2Int.zero;
        public Vector2Int Value
        {
            get { return _Value; }
            set
            {
                _Value = value;
                if (ChangeValueAlert)
                {
                    for (int i = 0; i < OnChangeBasicEvents.Count; i++)
                        OnChangeBasicEvents[i].Raise();
                }
            }
        }

#if UNITY_EDITOR
        [Multiline] public string Description = "";
#endif

        [Header("Value Change Event")]
        public bool ChangeValueAlert = false;
        public List<BasicEventHandler> OnChangeBasicEvents;

        public IntVector2Variable (Vector2Int _value)
        {
            OnChangeBasicEvents = new List<BasicEventHandler>();
            Value = _value;
        }

        public void RegisterEventHandler (BasicEventHandler _handler)
        {
            OnChangeBasicEvents.Add(_handler);
        }

        public bool UnregisterEventHandler (BasicEventHandler _handler)
        {
            return OnChangeBasicEvents.Remove(_handler);
        }
    }
}
