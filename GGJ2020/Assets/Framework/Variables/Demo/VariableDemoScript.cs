﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JTK;

public class VariableDemoScript : MonoBehaviour {

    [SerializeField] FloatReference floatVar;
    [SerializeField] IntReference intVar;
    [SerializeField] StringReference stringVar;
    [SerializeField] Vector2Reference vector2Var;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
