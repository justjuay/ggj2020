﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace JTK
{
    public interface IEventHandler<T>
    {
        void Raise (T _arg);
        void RegisterEvent (IEventListener<T> _listener);
        void UnregisterEvent (IEventListener<T> _listener);
    }

    [System.Serializable]
    public class GenericEventHandler<T> : ScriptableObject, IEventHandler<T>
    {
        [SerializeField] public T defaultValue;
        private readonly List<IEventListener<T>> eventListeners = new List<IEventListener<T>>();
        
        public void Raise (T _arg)
        {
            T arg = _arg == null ? defaultValue : _arg;
            // Reverse to be removable while iterating
            for (int i = eventListeners.Count - 1; i >= 0; i--)
            {
                eventListeners[i].OnEventRaise(arg);
            }
        }

        public void RegisterEvent (IEventListener<T> _listener)
        {
            if (!eventListeners.Contains(_listener))
                eventListeners.Add(_listener);
        }

        public void UnregisterEvent (IEventListener<T> _listener)
        {
            if (eventListeners.Contains(_listener))
                eventListeners.Remove(_listener);
        }
    }
}

