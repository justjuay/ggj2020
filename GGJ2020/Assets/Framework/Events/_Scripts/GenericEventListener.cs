﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;

namespace JTK
{
    //public class UnityEvent_Int :       UnityEvent<int> { }
    //public class UnityEvent_Float :     UnityEvent<float> { }
    //public class UnityEvent_String :    UnityEvent<string> { }

    //public class IntEventListener :     GenericEventListener<int, UnityEvent_Int> { }
    //public class FloatEventListener :   GenericEventListener<float, UnityEvent_Float> { };
    //public class StringEventListener :  GenericEventListener<string, UnityEvent_String> { };

    public interface IEventListener<T>
    {
        void OnEventRaise (T _param);
    }

    [System.Serializable]
    public class GenericEventListeners<T> : MonoBehaviour, IEventListener<T>
    {
        [SerializeField] public IEventHandler<T> Event;
        [SerializeField] public UnityEvent Response;
        [SerializeField] public UnityEvent<T> VarResponse;

        public void OnEnable ()
        {
            Event.RegisterEvent(this);
        }

        public void OnDisable ()
        {
            Event.UnregisterEvent(this);
        }

        public void OnEventRaise (T _param)
        {
            Response.Invoke();
            VarResponse.Invoke(_param);
        }
    }
}

