﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;

namespace JTK
{   
    [System.Serializable] public class UnityEvent_Collider2D : UnityEvent<Collider2D> { }

    public class Collider2DEventListener : MonoBehaviour
    {
        public Collider2DEventHandler Event;

        public UnityEvent BasicResponse;
        public UnityEvent_Collider2D IntResponse;

        public void OnEnable ()
        {
            if (Event != null)
                Event.RegisterListener(this);
        }

        public void OnDisable ()
        {
            if (Event != null)
                Event.UnregisterLister(this);
        }

        public void OnEventRaised (Collider2D _value)
        {
            BasicResponse.Invoke();
            IntResponse.Invoke(_value);
        }
    }
}