﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JTK
{
    [CreateAssetMenu(menuName = "JTK/Framework/Events/Collider2D")]
    [System.Serializable]
    public class Collider2DEventHandler : ScriptableObject
    {
        public float defaultValue;
        private readonly List<Collider2DEventListener> eventListeners = new List<Collider2DEventListener>();

        public void Raise (Collider2D _value = null)
        {
            for (int i = eventListeners.Count - 1; i >= 0; i--)
            {
                eventListeners[i].OnEventRaised(_value);
            }
        }

        public void RegisterListener (Collider2DEventListener _listener)
        {
            if (!eventListeners.Contains(_listener))
                eventListeners.Add(_listener);
        }

        public void UnregisterLister (Collider2DEventListener _listener)
        {
            if (eventListeners.Contains(_listener))
                eventListeners.Remove(_listener);
        }
    }
}

