﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JTK
{
    [CreateAssetMenu(menuName = "JTK/Framework/Events/Int")]
    [System.Serializable]
    public class IntEventHandler : ScriptableObject
    {
        public int defaultValue;
        private readonly List<IntEventListener> eventListeners = new List<IntEventListener>();

        public void Raise (int _value = 0)
        {
            int value = _value == 0 ? defaultValue : _value;
            for (int i = eventListeners.Count - 1; i >= 0; i--)
            {
                eventListeners[i].OnEventRaised(value);
            }
        }

        public void RegisterListener (IntEventListener _listener)
        {
            if (!eventListeners.Contains(_listener))
                eventListeners.Add(_listener);
        }

        public void UnregisterLister (IntEventListener _listener)
        {
            if (eventListeners.Contains(_listener))
                eventListeners.Remove(_listener);
        }
    }
}

