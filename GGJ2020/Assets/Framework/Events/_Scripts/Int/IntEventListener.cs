﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;

namespace JTK
{   
    [System.Serializable] public class UnityEvent_Int : UnityEvent<int> { }

    public class IntEventListener : MonoBehaviour
    {
        public IntEventHandler Event;

        public UnityEvent BasicResponse;
        public UnityEvent_Int IntResponse;

        public void OnEnable ()
        {
            if (Event != null)
                Event.RegisterListener(this);
        }

        public void OnDisable ()
        {
            if (Event != null)
                Event.UnregisterLister(this);
        }

        public void OnEventRaised (int _value)
        {
            BasicResponse.Invoke();
            IntResponse.Invoke(_value);
        }
    }
}