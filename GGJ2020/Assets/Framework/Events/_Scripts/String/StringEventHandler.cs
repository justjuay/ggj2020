﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JTK
{
    [CreateAssetMenu(menuName = "JTK/Framework/Events/String")]
    [System.Serializable]
    public class StringEventHandler : ScriptableObject
    {
        public string defaultValue;
        private readonly List<StringEventListener> eventListeners = new List<StringEventListener>();

        public void Raise (string _value = null)
        {
            string value = _value == null ? defaultValue : _value;
            for (int i = eventListeners.Count - 1; i >= 0; i--)
            {
                eventListeners[i].OnEventRaised(value);
            }
        }

        public void RegisterListener (StringEventListener _listener)
        {
            if (!eventListeners.Contains(_listener))
                eventListeners.Add(_listener);
        }

        public void UnregisterLister (StringEventListener _listener)
        {
            if (eventListeners.Contains(_listener))
                eventListeners.Remove(_listener);
        }
    }
}

