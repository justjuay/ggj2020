﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;

namespace JTK
{   
    [System.Serializable] public class UnityEvent_String : UnityEvent<string> { }

    public class StringEventListener : MonoBehaviour
    {
        public StringEventHandler Event;

        public UnityEvent BasicResponse;
        public UnityEvent_String IntResponse;

        public void OnEnable ()
        {
            if (Event != null)
                Event.RegisterListener(this);
        }

        public void OnDisable ()
        {
            if (Event != null)
                Event.UnregisterLister(this);
        }

        public void OnEventRaised (string _value)
        {
            BasicResponse.Invoke();
            IntResponse.Invoke(_value);
        }
    }
}