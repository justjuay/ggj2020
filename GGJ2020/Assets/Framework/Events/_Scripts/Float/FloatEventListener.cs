﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;

namespace JTK
{   
    [System.Serializable] public class UnityEvent_Float : UnityEvent<float> { }

    public class FloatEventListener : MonoBehaviour
    {
        public FloatEventHandler Event;

        public UnityEvent BasicResponse;
        public UnityEvent_Float IntResponse;

        public void OnEnable ()
        {
            if (Event != null)
                Event.RegisterListener(this);
        }

        public void OnDisable ()
        {
            if (Event != null)
                Event.UnregisterLister(this);
        }

        public void OnEventRaised (float _value)
        {
            BasicResponse.Invoke();
            IntResponse.Invoke(_value);
        }
    }
}