﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JTK
{
    [CreateAssetMenu(menuName = "JTK/Framework/Events/Float")]
    [System.Serializable]
    public class FloatEventHandler : ScriptableObject
    {
        public float defaultValue;
        private readonly List<FloatEventListener> eventListeners = new List<FloatEventListener>();

        public void Raise (float _value = 0f)
        {
            float value = _value == 0f ? defaultValue : _value;
            for (int i = eventListeners.Count - 1; i >= 0; i--)
            {
                eventListeners[i].OnEventRaised(value);
            }
        }

        public void RegisterListener (FloatEventListener _listener)
        {
            if (!eventListeners.Contains(_listener))
                eventListeners.Add(_listener);
        }

        public void UnregisterLister (FloatEventListener _listener)
        {
            if (eventListeners.Contains(_listener))
                eventListeners.Remove(_listener);
        }
    }
}

