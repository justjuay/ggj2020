﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JTK
{
    [CreateAssetMenu(menuName = "JTK/Framework/Events/Basic")]
    public class BasicEventHandler : ScriptableObject
    {
        private readonly List<BasicEventListener> eventListeners = new List<BasicEventListener>();
	
        public void Raise ()
        {
            // Reverse to be removable while iterating
            for (int i = eventListeners.Count - 1; i >= 0; i--)
            {
                eventListeners[i].OnEventRaised();
            }
        }
        
        public void RegisterListener (BasicEventListener _listener)
        {
            if (!eventListeners.Contains(_listener))
                eventListeners.Add(_listener);
        }

        public void UnregisterListener (BasicEventListener _listener)
        {
            if (eventListeners.Contains(_listener))
                eventListeners.Remove(_listener);
        }
    }
    

    
}

