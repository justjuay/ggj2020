﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JTK
{
    [CreateAssetMenu(menuName = "JTK/Framework/Events/Vector3")]
    [System.Serializable]
    public class Vector3EventHandler : ScriptableObject
    {
        private readonly List<Vector3EventListener> eventListeners = new List<Vector3EventListener>();

        public void Raise (Vector3? _value = null)
        {
            for (int i = eventListeners.Count - 1; i >= 0; i--)
            {
                eventListeners[i].OnEventRaised(_value);
            }
        }
        
        public void RegisterListener (Vector3EventListener _listener)
        {
            if (!eventListeners.Contains(_listener))
                eventListeners.Add(_listener);
        }

        public void UnregisterListener (Vector3EventListener _listener)
        {
            if (eventListeners.Contains(_listener))
                eventListeners.Remove(_listener);
        }
    }
}

