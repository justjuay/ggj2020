﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;

namespace JTK
{
    [System.Serializable] public class UnityEvent_Vector3 : UnityEvent<Vector3?> { }

    public class Vector3EventListener : MonoBehaviour
    {
        public Vector3EventHandler Event;

        public UnityEvent BasicResponse;
        public UnityEvent_Vector3 Vector3Response;

        public void OnEnabled ()
        {
            if (Event != null)
                Event.RegisterListener(this);
        }

        public void OnDisable ()
        {
            if (Event != null)
                Event.UnregisterListener(this);
        }

        public void OnEventRaised (Vector3? _value)
        {
            BasicResponse.Invoke();
            Vector3Response.Invoke(_value);
        }
    }
}


