﻿using UnityEditor;
using UnityEngine;

namespace JTK
{
    [CustomEditor(typeof(BasicEventHandler))]
    public class EventHandlerEditor : Editor
    {
        public override void OnInspectorGUI ()
        {
            base.OnInspectorGUI();

            GUI.enabled = Application.isPlaying;

            BasicEventHandler handler = target as BasicEventHandler;
            if (GUILayout.Button("Raise"))
                handler.Raise();
        }
    }
}
