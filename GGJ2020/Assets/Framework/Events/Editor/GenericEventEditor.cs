﻿using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

namespace JTK
{
    [CustomEditor(typeof(IntEventHandler))]
    public class IntEventHandlerEditor : GenericEventHandlerEditor<int> { }

    [CustomEditor(typeof(FloatEventHandler))]
    public class FloatEventHandlerEditor : GenericEventHandlerEditor<float> { }

    [CustomEditor(typeof(StringEventHandler))]
    public class StringEventHandlerEditor : GenericEventHandlerEditor<string> { }


    public class GenericEventHandlerEditor<T> : Editor
    {
        public override void OnInspectorGUI ()
        {
            base.OnInspectorGUI();

            GUI.enabled = Application.isPlaying;

            GenericEventHandler<T> handler = target as GenericEventHandler<T>;
            if (GUILayout.Button("Raise"))
                handler.Raise(default(T));
        }
    }
}
