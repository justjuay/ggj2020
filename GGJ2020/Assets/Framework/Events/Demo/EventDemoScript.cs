﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using JTK;

public class EventDemoScript : MonoBehaviour
{
    int counter = 0;
    [SerializeField] Text txt;

    public void IncrementCounter ()
    {
        counter++;
        txt.text = counter.ToString();
    }

}
