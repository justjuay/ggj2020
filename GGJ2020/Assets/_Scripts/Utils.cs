﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Utils : MonoBehaviour
{
    public static Quaternion LookAt(Vector3 dir)
    {
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        angle -= 90;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        return q;
    }

    public static void CameraShake(float intensity = 0.1f, float duration = 0.1f)
    {
        Camera.main.transform.DOComplete();
        Camera.main.transform.DOShakePosition(duration, intensity, 100, 90, false, true);
    }
}
