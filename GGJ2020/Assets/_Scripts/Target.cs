﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using JTK;
class Target : MonoBehaviour
{
    [SerializeField] int leakDmgPerHole;
    public List<Node> nodes;
    public Material healthMaterial;

    public Sprite healthySprite;
    public Sprite dyingSprite;

    public Sprite deadSprite;
    public Sprite winSprite;

    public Sprite hitSprite;
    public Sprite healSprite;

    public GameObject rippleObj;
    [SerializeField] SpriteRenderer renderer;

    public SpriteRenderer pedestalRenderer;

    [SerializeField] float leakHeathPerSec;
    [SerializeField] float dyingThreshold;

    [SerializeField] List<GameObject> pillars;
    [SerializeField] Animator anim;
    int spawnPillarIndex = 0;

    Coroutine newBreakRoutine = null;
    Coroutine newRepairRoutine = null;

    [SerializeField] GameObject emoticonHit;
    [SerializeField] GameObject emoticonRepair;
    [SerializeField] GameObject emoticonVictory;

    [SerializeField] Image healthImg;

    bool isDying = false;
    float _totalHealth = 1.0f;
    public float totalHealth
    {
        get { return _totalHealth; }
        set 
        { 
            _totalHealth = value; 
            if (!isDying && _totalHealth <= dyingThreshold)
            {
                renderer.sprite = dyingSprite;
                isDying = true;
            }

            if (GameManager.instance.hasGameStarted)
                healthImg.fillAmount = _totalHealth;
            else
                healthImg.fillAmount = 0;
            //if (_totalHealth > dyingThreshold && isDying)
            //{
            //    renderer.sprite = healthySprite;
            //    isDying = false;
            //}
        }
    }

    private void Awake()
    {

    }

    void Start()
    {
        Reset();
    }

    private void Update()
    {
        if (!GameManager.instance.hasGameStarted) return;

        List<Node> temp = nodes.FindAll(x => x.isBroken);
        int amountOfHoles = temp == null ? 0 : temp.Count;
        if (amountOfHoles > 0 && totalHealth > 0.0f)
        {
            totalHealth -= amountOfHoles * leakHeathPerSec * Time.deltaTime;
        }

        if (totalHealth < 0.7f)
        {
            float remapValue = 1f- Remap(1f - totalHealth, 0.0f, 0.7f, 0.0f, 0.35f);
            
            pedestalRenderer.color = new Color(remapValue, remapValue, remapValue);
        }

        if (totalHealth < 0.5f)
        {
            healthMaterial.SetFloat("_scale", 1f - Remap(totalHealth, 0.5f, 1.0f, 0f, 1f));
        }

        float scaleValue = Remap(totalHealth, 0.0f, 1.0f, 0.4f, 1.0f);
        this.transform.localScale = new Vector3(scaleValue, scaleValue, scaleValue);

        healthMaterial.SetFloat("_scale", 1.0f - totalHealth);
    }

    private void OnDestroy()
    {
        healthMaterial.SetFloat("_scale", 0f);
    }

    public List<Node> getUnbroken()
    {
        List<Node> unbroken = new List<Node>();
        foreach (Node n in nodes)
        {
            if (!n.isBroken)
                unbroken.Add(n);
        }
        return unbroken;
    }

    public bool SpawnPillar ()
    {
        pillars[spawnPillarIndex].SetActive(true);
        spawnPillarIndex++;
        return spawnPillarIndex >= pillars.Count;
    }

    public void BreakNode (Node n)
    {
        if (nodes.Contains(n))
        {
            n.Break();
            showHitFx();
        }
        else
            Debug.LogError("Tried to break unowned node");
    }

    public void showHitFx()
    {
        if (newBreakRoutine == null)
            newBreakRoutine = StartCoroutine(newBreak_Routine());
    }

    public void showRepairFX()
    {
        if (newRepairRoutine == null)
        {
            newRepairRoutine = StartCoroutine(newRepair_Routine());
        }
    }

    IEnumerator newRepair_Routine()
    {
        //Set face
        emoticonRepair.SetActive(true);
        renderer.sprite = healSprite;
        anim.SetTrigger("RepairTrigger");
        yield return new WaitForSeconds(0.35f);
        renderer.sprite = isDying ? dyingSprite : healthySprite;
        anim.SetTrigger(isDying ? "DyingTrigger" : "IdleTrigger");
        newRepairRoutine = null;
    }

    public void Reset()
    {
        spawnPillarIndex = 0;
        isDying = false;
        totalHealth = 1.0f;
        foreach (Node n in nodes)
        {
            n.Reset();
        }
        foreach (GameObject p in pillars)
        {
            p.SetActive(false);
        }

        healthMaterial.SetFloat("_scale", 0);
        renderer.sprite = healthySprite;
        anim.SetTrigger("IdleTrigger");
        emoticonVictory.SetActive(false);
        this.transform.localScale = Vector3.one;
    }

    IEnumerator newBreak_Routine()
    {
        emoticonHit.SetActive(true);
        if (isDying)
        {
            rippleObj.SetActive(true);
            AudioManager.instance.PlayOneShot(AudioManager.Clips.Water);
        }
        renderer.sprite = hitSprite;
        anim.SetTrigger("HitTrigger");
        yield return new WaitForSeconds(0.35f);
        renderer.sprite = isDying ? dyingSprite : healthySprite;
        anim.SetTrigger(isDying ? "DyingTrigger" : "IdleTrigger");
        newBreakRoutine = null;
    }

    public void setDeadFace(bool isDeadFace)
    {
        renderer.sprite = isDeadFace ? deadSprite : winSprite;
        anim.SetTrigger(isDeadFace ? "DyingTrigger" : "VictoryTrigger");
        if (!isDeadFace)
        {
            emoticonVictory.SetActive(true);
        }
    }

    float Remap(float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }
}

