﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stitch : MonoBehaviour
{
    public float speed;
    public GameObject hitWallFx;

    List<Vector2> targets;
    int currentTargetIndex = 0;
    bool isMoving = false;
    System.Action onFinishCallback;

    bool isActive = true;
    bool destroyOnStop = false;

    // Called by Balloon
    public bool Hit()
    {
        if (onFinishCallback != null)
        {
            onFinishCallback();
        }

        if (isActive)
        {
            isActive = false;
            Destroy(gameObject);
            return true;
        }
        return false;
    }

    public void Kill()
    {
        isActive = false;
        isMoving = false;
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Balloon")
        {
            Hit();
            collision.GetComponent<Node>().Repair();
        }
    }

    public void SetDestroyOnStop(bool destroyOnStop)
    {
        this.destroyOnStop = destroyOnStop;
    }

    public void SetCallback(System.Action callback)
    {
        onFinishCallback = callback;
    }

    public void SetTargets(List<Vector2> positions)
    {
        targets = positions;
    }

    public void StartMoving()
    {
        isMoving = true;
        isActive = true;
        currentTargetIndex = 0;
        transform.rotation = Utils.LookAt(targets[currentTargetIndex] - (Vector2) transform.position);
    }

    void StopMoving()
    {
        isMoving = false;
        isActive = false;
        GetComponent<Collider2D>().enabled = false;
        if (onFinishCallback != null)
        {
            onFinishCallback();
        }

        if (destroyOnStop)
        {
            Kill();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (isMoving)
        {
            if (currentTargetIndex >= targets.Count)
            {
                // No more targets. Just stay there forever lol.
                StopMoving();
                return;
            }

            Vector2 nextPosition = targets[currentTargetIndex];
            float distance = Vector2.Distance(transform.position, nextPosition);
            float step = speed * Time.deltaTime;
            if (Mathf.Abs(distance) <= step)
            {
                // Reached
                ++currentTargetIndex;
                if (currentTargetIndex < targets.Count)
                {
                    GameObject fx = Instantiate(hitWallFx);
                    fx.transform.position = transform.position;
                    transform.rotation = Utils.LookAt(targets[currentTargetIndex] - (Vector2)transform.position);
                    AudioManager.instance.PlayOneShot(AudioManager.Clips.HitWall);
                }
            }

            transform.position = Vector2.MoveTowards(transform.position, nextPosition, step);
        }
    }
}
