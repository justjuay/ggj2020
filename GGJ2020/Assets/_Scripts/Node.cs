﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Node : MonoBehaviour
{
    [HideInInspector] public bool isBroken;
    [SerializeField] GameObject leakingFX;
    [SerializeField] GameObject stitch;
    [SerializeField] public int index;

    Target parentTarget;

    private void Awake()
    {
        parentTarget = this.GetComponentInParent<Target>();
    }


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Repair()
    {
        if (!isBroken)
        {
            parentTarget.showHitFx();
            AudioManager.instance.PlayOneShot(AudioManager.Clips.BalloonHole);
            AudioManager.instance.PlayOneShot(AudioManager.Clips.Water);
        }
        else
        {
            isBroken = false;
            StartCoroutine(Repair_Routine());
        }

    }

    IEnumerator Repair_Routine()
    {
        parentTarget.showRepairFX();
        leakingFX.SetActive(false);
        stitch.SetActive(true);
        AudioManager.instance.PlayOneShot(AudioManager.Clips.Repair);
        yield return new WaitForSeconds(4.0f) ;
        stitch.SetActive(false);
    }

    public void Break()
    {
        isBroken = true;
        StartCoroutine(Break_Coroutine());
    }

    private IEnumerator Break_Coroutine()
    {
        leakingFX.SetActive(true);
        yield return new WaitForSeconds(2.0f);
    }

    public void Reset()
    {
        isBroken = false;
        leakingFX.SetActive(false);
        stitch.SetActive(false);
    }

}
