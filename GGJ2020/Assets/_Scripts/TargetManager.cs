﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

public class TargetManager : MonoBehaviour
{
    public static TargetManager instance;

    [SerializeField] Target leftTarget;
    [SerializeField] Target rightTarget;


    [SerializeField] Vector2 lvl1_breakInterval;
    [SerializeField] Vector2 lvl1_breakCount;
    [SerializeField] int lvl1_loopCount;

    [SerializeField] Vector2 lvl2_breakInterval;
    [SerializeField] Vector2 lvl2_breakCount;
    [SerializeField] int lvl2_loopCount;

    Coroutine gameRoutine;
    Coroutine pillarRoutine;

    // Use this for initialization
    void Start()
    {
        if (instance == null)
            instance = this;


    }

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.instance.hasGameStarted) return;

        if (leftTarget.totalHealth <= 0)
        {
            GameManager.instance.Winning(false);
            leftTarget.setDeadFace(true);
            rightTarget.setDeadFace(false);
        }
        else if (rightTarget.totalHealth <= 0)
        {
            GameManager.instance.Winning(true);
            leftTarget.setDeadFace(false);
            rightTarget.setDeadFace(true);
        }
        else
            return;


        StopCoroutine(gameRoutine);
        gameRoutine = null;

        StopCoroutine(pillarRoutine);
        pillarRoutine = null;
    }

    public void startGame()
    {
        gameRoutine = StartCoroutine(Game_Routine());
        pillarRoutine = StartCoroutine(Pillar_Routine());
    }

    public void getReady()
    {

        leftTarget.Reset();
        rightTarget.Reset();
    }

    public void CreateNewLeak(int leakCount)
    {
        List<Node> leftCandidates = leftTarget.getUnbroken();
        List<Node> rightCandidates = rightTarget.getUnbroken();

        if (leftCandidates.Count <= 0 || rightCandidates.Count <= 0)
            return;

        List<int> candidates = new List<int>();
        foreach (Node n in leftCandidates)
        {
            if (rightCandidates.Find(x => x.index == n.index) != null) {
                candidates.Add(n.index);
            }
        }

        if (candidates.Count <= 0) return;

        for (int i = 0; i < leakCount; i++)
        {
            int choosen = Random.Range(0, candidates.Count);
            
            Node leftNode = leftCandidates.Find(x => x.index == candidates[choosen]);
            leftTarget.BreakNode(leftNode);
            Node rightNode = rightCandidates.Find(x => x.index == candidates[choosen]);
            rightTarget.BreakNode(rightNode);

            candidates.RemoveAt(choosen);
            if (candidates.Count <= 0) break;
        }

        AudioManager.instance.PlayOneShot(AudioManager.Clips.BalloonHole);
        AudioManager.instance.PlayOneShot(AudioManager.Clips.Water);
        Utils.CameraShake(0.65f, 0.4f);
    }

    IEnumerator Game_Routine()
    {
        int count = 0;
        do
        {
            CreateNewLeak((int)Random.Range(lvl1_breakCount.x, lvl1_breakCount.y));
            yield return new WaitForSeconds
                (Random.Range(lvl1_breakInterval.x, lvl1_breakInterval.y));
            count++;
        } while (count < lvl1_loopCount);

        count = 0;
        do
        {
            CreateNewLeak((int)Random.Range(lvl2_breakCount.x, lvl2_breakCount.y));
            yield return new WaitForSeconds
                (Random.Range(lvl2_breakInterval.x, lvl2_breakInterval.y));
            count++;
        } while (count < lvl2_loopCount);
    }

    IEnumerator Pillar_Routine()
    {
        yield return new WaitForSeconds(20f);

        bool leftCanSpawn = true, rightCanSpawn = true;

        while (leftCanSpawn && rightCanSpawn)
        {
            leftCanSpawn = !leftTarget.SpawnPillar();
            rightCanSpawn = !rightTarget.SpawnPillar();
            AudioManager.instance.PlayOneShot(AudioManager.Clips.PillarFall);
            yield return new WaitForSeconds(0.3f);
            AudioManager.instance.PlayOneShot(AudioManager.Clips.WaterSplashIn);
            yield return new WaitForSeconds(15f);
        }
            
    }
}
