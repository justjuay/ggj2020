﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    public Player player;

    public PLAYER_NUM playerNumber;

    public enum PLAYER_NUM
    {
        One,
        Two
    }

    // Start is called before the first frame update
    void Start()
    {
        if (player == null) {
            player = GetComponent<Player>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.instance.hasGameStarted) return;

        if (playerNumber == PLAYER_NUM.One)
        {
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                player.MovePosition();
            } else if (Input.GetKey(KeyCode.RightArrow))
            {
                player.AimRight();
            } else if (Input.GetKey(KeyCode.LeftArrow))
            {
                player.AimLeft();
            } else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                player.Shoot();
            } else if (Input.GetKeyUp(KeyCode.RightArrow) || Input.GetKeyUp(KeyCode.LeftArrow))
            {
                player.ResetSpeed();
            }
        } else
        {
            if(Input.GetKeyDown(KeyCode.S))
            {
                player.MovePosition();
            }
            else if (Input.GetKey(KeyCode.D))
            {
                player.AimRight();
            }
            else if (Input.GetKey(KeyCode.A))
            {
                player.AimLeft();
            }
            else if (Input.GetKeyDown(KeyCode.W))
            {
                player.Shoot();
            }
            else if (Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D))
            {
                player.ResetSpeed();
            }
        }
    }
}
