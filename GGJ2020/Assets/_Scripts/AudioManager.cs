﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;

    private AudioSource sfx;
    private AudioSource bgm;

    public List<Clip> map;

    [System.Serializable]
    public struct Clip
    {
        public Clips clip;
        public AudioClip[] audio;

    }

    public enum Clips
    {
        Start,
        Shoot,
        Repair,
        PillarFall,
        HitWall,
        BalloonHole,
        Water,
        WaterSplashIn
    }

    public void PlayOneShot(Clips clip)
    {
        AudioClip[] sounds = map.Find(x => x.clip == clip).audio;
        sfx.PlayOneShot(sounds[Random.Range(0, sounds.Length)]);
    }

    public void StartGame()
    {
        //bgm.Play();
    }

    public void StopGame()
    {
        //bgm.Stop();
    }

    private void Awake()
    {
        if (instance == null)
            instance = this;

        AudioSource[] sources = GetComponentsInChildren<AudioSource>();
        bgm = sources[0];
        sfx = sources[1];
    }

}
