﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [Header("Initializations")]
    public Stitch stitchPrefab;
    public Transform [] positions;
    public Transform nozzle;
    public SpriteRenderer[] lines;
    public Color[] colors; // for debug lines only for now
    public GameObject lightAnimation;
    public Color canShootColor;
    public Color cannotShootColor;
    public GameObject shootFx;


    [Header("Settings")]
    public float speedOfAimingNozzle = 100;
    public float maxLineLength = 15f;
    public float accelerationOfAiming = 5f;
    public float cooldown = 0.5f;

    float actualSpeedOfAiming = 0;

    Vector3[] savedPositions;
    int currentPosition; // 0 - left, 1 - middle or 2 - right
    int countLines = 0;
    bool canHitBalloon = true;
    bool canShoot = true;
    bool canShowLine = true;
    GameObject lineParent;

    public void StartGame()
    {
        lineParent.SetActive(true);
    }

    public void StopGame()
    {
        lineParent.SetActive(false);
        ResetPosition();
        ResetAim();
    }

    void ResetPosition()
    {
        currentPosition = 1;
        transform.position = savedPositions[currentPosition];
    }

    public void MovePosition(bool reset = false)
    {
        ++currentPosition;

        if (currentPosition >= positions.Length)
        {
            currentPosition = 0;
        }

        if (reset) currentPosition = 1;

        transform.position = savedPositions[currentPosition];
        ResetAim();
        PlayLaneLightAnim();
    }

    public void ResetAim()
    {
        nozzle.transform.rotation = Quaternion.identity;
    }

    public void AimLeft()
    {
        AdjustSpeed();

        float angle = nozzle.transform.rotation.eulerAngles.z;
        if (angle > 100 || angle < 85)
        {
            nozzle.transform.Rotate(Vector3.forward * actualSpeedOfAiming * Time.deltaTime);
        }
    }

    public void AimRight()
    {
        AdjustSpeed();

        float angle = nozzle.transform.rotation.eulerAngles.z;
        if (angle < 260 || angle > 275)
        {
            nozzle.transform.Rotate(-Vector3.forward * actualSpeedOfAiming * Time.deltaTime);
        }
    }

    void AdjustSpeed()
    {
        actualSpeedOfAiming += accelerationOfAiming;
        actualSpeedOfAiming = Mathf.Clamp(actualSpeedOfAiming, 0, speedOfAimingNozzle);
    }

    public void ResetSpeed()
    {
        actualSpeedOfAiming = 0;
    }

    public void Shoot()
    {
        if (canShoot)
        {
            canShoot = false;
            canShowLine = false;

            List<Vector2> targets = new List<Vector2>();
            for (int i = 1; i < lines.Length; ++i)
            {
                if (i > countLines) break;

                targets.Add(lines[i].transform.position);
            }

            Stitch stitch = Instantiate(stitchPrefab);
            stitch.transform.position = nozzle.position;
            stitch.transform.rotation = nozzle.rotation;
            stitch.SetDestroyOnStop(canHitBalloon);
            stitch.SetTargets(targets);
            stitch.StartMoving();

            DisableLines();

            StartCoroutine(ResetShoot());

            Utils.CameraShake();
            shootFx.SetActive(true);
            AudioManager.instance.PlayOneShot(AudioManager.Clips.Shoot);
        }
    }

    IEnumerator ResetShoot()
    {
        float timeForLineToComeBack = 0.15f;
        yield return new WaitForSeconds(timeForLineToComeBack);

        canShowLine = true;

        yield return new WaitForSeconds(cooldown - timeForLineToComeBack);
        canShoot = true;
    }

    // Start is called before the first frame update
    void Awake()
    {
        savedPositions = new Vector3[positions.Length];
        for (int i = 0; i < positions.Length; ++i)
        {
            Transform t = positions[i];
            savedPositions[i] = t.position;
        }

        ResetPosition();
        ResetAim();

        lineParent = lines[0].transform.parent.gameObject;
        lineParent.SetActive(false);
    }

    void ShootRay(int lineNum, float currentLength)
    {
        if (lineNum == lines.Length - 1) return; // Last one

        lines[lineNum].gameObject.SetActive(true);
        lines[lineNum].color = canShoot ? canShootColor : cannotShootColor;

        Vector2 dir = lines[lineNum].transform.up;
        // move origin a little bit forward to prevent hitting self
        Vector2 origin = lines[lineNum].transform.position + (Vector3) dir.normalized * 0.1f;

        Debug.DrawRay(origin, dir, colors[lineNum], 1f);
        RaycastHit2D hit = Physics2D.Raycast(origin, dir);

        if (hit.collider != null)
        {
            float distance = Vector2.Distance(lines[lineNum].transform.position, hit.point);
            if ((currentLength + distance) > maxLineLength)
            {
                // Reached max distance for line. Adjust distance to only show until max length.
                distance = maxLineLength - currentLength;
            }
            lines[lineNum].setTileHeight(distance);



            if (lineNum + 1 >= lines.Length)
            { // no more next line
                return;
            }


            SpriteRenderer nextLine = lines[lineNum + 1];
            Vector2 reflectDir = Vector2.Reflect(lines[lineNum].transform.up, hit.normal);
            //Debug.DrawRay(nextLine.transform.position, reflectDir, Color.green, 1f);
            nextLine.transform.rotation = Utils.LookAt(reflectDir);
            nextLine.transform.position = hit.point;
            countLines = lineNum + 1;

            if (hit.collider.tag == "Wall")
            {
                canHitBalloon = false;
                ShootRay(lineNum + 1, currentLength + distance);
            } else if (hit.collider.tag == "Balloon")
            {
                canHitBalloon = true;
                DisableLines(lineNum + 1);
            }
        }
    }

    void PlayLaneLightAnim()
    {
        lightAnimation.SetActive(false);
        lightAnimation.SetActive(true);
    }

    void DisableLines(int startIndex = 0)
    {
        for (int i = startIndex; i < lines.Length; ++i)
        {
            lines[i].gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        lines[0].transform.position = nozzle.position;
        lines[0].transform.rotation = nozzle.rotation;
        ShootRay(0, 0);
    }
}
