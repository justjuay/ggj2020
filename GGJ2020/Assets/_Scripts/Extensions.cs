﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extensions
{
    public static void setTileHeight(this SpriteRenderer sprite, float tileHeight)
    {
        Vector2 v = sprite.size;
        v.y = tileHeight;
        sprite.size = v;
    }
}
