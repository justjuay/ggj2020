﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    [SerializeField] Player player1;
    [SerializeField] Player player2;
    [SerializeField] TargetManager targetMgr;
    [Header("UI")]
    [SerializeField] GameObject instructionUI;
    [SerializeField] GameObject leftWinner;
    [SerializeField] GameObject rightWinner;

    [HideInInspector] public bool hasGameStarted = false;

    Coroutine endGameRoutine;
    private void Awake()
    {
        if (instance == null)
            instance = this;

    }
    void Start()
    {
        Init();

    }

    // Update is called once per frame
    void Update()
    {
        if (!hasGameStarted)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                StartGame();
            }
        }
    }

    private void Init()
    {
        instructionUI.SetActive(true);
        leftWinner.SetActive(false);
        rightWinner.SetActive(false);
        player1.StopGame();
        player2.StopGame();
        
    }

    private void StartGame()
    {
        hasGameStarted = true;
        if (endGameRoutine != null)
        {
            StopCoroutine(endGameRoutine);
            endGameRoutine = null;
        }
        instructionUI.SetActive(false);
        leftWinner.SetActive(false);
        rightWinner.SetActive(false);
        player1.StartGame();
        player2.StartGame();
        targetMgr.getReady();
        targetMgr.startGame();
        AudioManager.instance.StartGame();
    }

    public void Winning(bool isLeft)
    {
        hasGameStarted = false;
        player1.StopGame();
        player2.StopGame();
        AudioManager.instance.StopGame();
        foreach (Stitch s in FindObjectsOfType<Stitch>())
        {
            s.Kill();
        }
        endGameRoutine = StartCoroutine(Winning_Routine(isLeft));
    }
    
    IEnumerator Winning_Routine(bool isLeft)
    {
        leftWinner.SetActive(isLeft);
        rightWinner.SetActive(!isLeft);
        AudioManager.instance.PlayOneShot(AudioManager.Clips.Start);
        yield return new WaitForSeconds(60.0f);
        leftWinner.SetActive(false);
        rightWinner.SetActive(false);
        instructionUI.SetActive(true);
        targetMgr.getReady();
        endGameRoutine = null;
    }


}
